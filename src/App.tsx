import { useRef, Suspense } from "react";
import { HexColorPicker } from "react-colorful";
import { useGLTF, OrbitControls, Environment } from "@react-three/drei";
import { Canvas, useFrame } from "react-three-fiber";
import { proxy, useSnapshot } from "valtio";
import "./App.css";

const state = proxy({
  current: null,
  items: {
    laces: "#ffffff",
    outsole: "#ffffff",
    lambert_outsole: "#ffffff",
    tissu_rigid: "#ffffff",
    lambert_tongue: "#ffffff",
    tissu_tongue: "#ffffff",
    rigid: "#ffffff",
    supports: "#ffffff",
    tongue: "#ffffff",
    top_cover: "#ffffff",
  },
});

function Shoejoinded() {
  const group = useRef();
  const snap = useSnapshot(state);
  const { nodes, materials } = useGLTF(
    "/assets/meshes/sneaker_nike_joined.glb"
  );
  // const [hovered, set] = useState(null);
  // //@ts-ignore
  // useEffect(() => {
  //   const cursor = `<svg width="64" height="64" fill="none" xmlns="http://www.w3.org/2000/svg"><g clip-path="url(#clip0)"><path fill="rgba(255, 255, 255, 0.5)" d="M29.5 54C43.031 54 54 43.031 54 29.5S43.031 5 29.5 5 5 15.969 5 29.5 15.969 54 29.5 54z" stroke="#000"/><g filter="url(#filter0_d)"><path d="M29.5 47C39.165 47 47 39.165 47 29.5S39.165 12 29.5 12 12 19.835 12 29.5 19.835 47 29.5 47z" fill="${
  //     snap.items[hovered!]
  //   }"/></g><path d="M2 2l11 2.947L4.947 13 2 2z" fill="#000"/></g><defs><clipPath id="clip0"><path fill="#fff" d="M0 0h64v64H0z"/></clipPath><filter id="filter0_d" x="6" y="8" width="47" height="47" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feColorMatrix in="SourceAlpha" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/><feOffset dy="2"/><feGaussianBlur stdDeviation="3"/><feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.15 0"/><feBlend in2="BackgroundImageFix" result="effect1_dropShadow"/><feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape"/></filter></defs></svg>`;
  //   const auto = `<svg width="64" height="64" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill="rgba(255, 255, 255, 0.5)" d="M29.5 54C43.031 54 54 43.031 54 29.5S43.031 5 29.5 5 5 15.969 5 29.5 15.969 54 29.5 54z" stroke="#000"/><path d="M2 2l11 2.947L4.947 13 2 2z" fill="#000"/></svg>`;
  //   if (hovered) {
  //     document.body.style.cursor = `url('data:image/svg+xml;base64,${btoa(
  //       cursor
  //     )}'), auto`;
  //     return () =>
  //       (document.body.style.cursor = `url('data:image/svg+xml;base64,${btoa(
  //         auto
  //       )}'), auto`);
  //   }
  // }, [hovered]);

  useFrame((state) => {
    const t = state.clock.getElapsedTime();
    //@ts-ignore
    group.current.rotation.z = -0.2 - (1 + Math.sin(t / 1.5)) / 20;
  });

  return (
    <group
      ref={group}
      dispose={null}
      onPointerOver={(e) => {
        e.stopPropagation();
        //@ts-ignore
        set(e.object.material.name);
      }}
      // onPointerOut={(e) => {
      //   e.intersections.length === 0 && set(null);
      // }}
      onPointerDown={(e) => {
        e.stopPropagation();
        //@ts-ignore
        console.log(e.object.material.name);
        //@ts-ignore
        state.current = e.object.material.name;
      }}
      onPointerMissed={(e) => {
        state.current = null;
      }}
    >
      <group rotation={[Math.PI / -0.3, 0, Math.PI / -1.4]} scale={0.01}>
        <mesh
          material-color={snap.items.laces}
          geometry={nodes.shoe_1.geometry}
          material={materials.laces}
        />
        <mesh
          material-color={snap.items.outsole}
          geometry={nodes.shoe_2.geometry}
          material={materials.outsole}
        />
        <mesh
          material-color={snap.items.lambert_outsole}
          geometry={nodes.shoe_3.geometry}
          material={materials.lambert_outsole}
        />
        <mesh
          material-color={snap.items.tissu_rigid}
          geometry={nodes.shoe_4.geometry}
          material={materials.tissu_rigid}
        />
        <mesh
          material-color={snap.items.supports}
          geometry={nodes.shoe_5.geometry}
          material={materials.supports}
        />
        <mesh
          material-color={snap.items.lambert_tongue}
          geometry={nodes.shoe_6.geometry}
          material={materials.lambert_tongue}
        />
        <mesh
          material-color={snap.items.tissu_tongue}
          geometry={nodes.shoe_7.geometry}
          material={materials.tissu_tongue}
        />
        <mesh
          material-color={snap.items.top_cover}
          geometry={nodes.shoe_8.geometry}
          material={materials.top_cover}
        />
      </group>
    </group>
  );
}

function Picker() {
  const snap = useSnapshot(state);
  return (
    <div
      className="container_picker"
      style={{ display: snap.current ? "block" : "none" }}
    >
      <HexColorPicker
        className="picker"
        color={snap.items[snap.current!]}
        //@ts-ignore
        onChange={(color) => (state.items[snap.current] = color)}
      />
      <h2>{snap.current}</h2>
    </div>
  );
}

export default function App() {
  return (
    <>
      <h1 className="title">
        Click on the shoe's component and choose a color
      </h1>
      <Picker />
      <Canvas camera={{ position: [0, 0, 0.2], fov: 50, near: 0.03 }}>
        <ambientLight intensity={0.5} />
        <spotLight intensity={0.5} position={[5, 20, 20]} />
        <Suspense fallback={null}>
          <Shoejoinded />
          <Environment files="assets/city_night.hdr" />
        </Suspense>
        <OrbitControls />
      </Canvas>
    </>
  );
}
